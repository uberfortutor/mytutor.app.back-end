import { BaseService } from './base.service';
import { Contract } from '../database/entities/contract.entity';

class ContractService extends BaseService<Contract> {
  constructor() {
    super(Contract);
  }

  async isValidToUpdate(id: number) {
    return this.isExists({ id, status: 0, delete: false }, 'id');
  }
}

export const contractService = new ContractService();
