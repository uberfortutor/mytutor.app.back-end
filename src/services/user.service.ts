import { Repository, Transaction, TransactionRepository, In } from 'typeorm';
import * as jwt from 'jsonwebtoken';
import * as bcrypt from 'bcrypt';
import { get, omit, hasIn } from 'lodash';
import { User } from '../database/entities/user.entity';
import { hashPassword } from '../utils/hash-password.util';
import { USER_ROLES, GENDERS } from '../commons/defines/enums';
import { Tutor } from '../database/entities/tutor.entity';
import { UserAddress } from '../database/entities/user-address.entity';
import { mailService } from './mail.service';
import { BaseService } from './base.service';
import { JWTConfig } from '../commons/configs';
import { commonConfigs } from '../commons/configs/common.config';
import { removeUndefinedField } from '../utils/object.util';
import { tutorService } from './tutor.service';
import { Contract } from '../database/entities/contract.entity';
import { contractService } from './contract.service';
import { Skill } from '../database/entities/skill.entity';
import { skillService } from './skill.service';
import {
  USER_FIELDS_NEED_TO_REMOVE,
  CONTRACT_FIELDS_NEED_TO_GET,
} from '../commons/defines/constants';

class UserService extends BaseService<User> {
  constructor() {
    super(User);
  }

  private createAuthorizationToken(data: any): string {
    return jwt.sign(Object.assign({}, data), JWTConfig.SECRET_KEY);
  }

  async signUp(data: any, socialAuth = false): Promise<number> {
    this.getRepository();
    const address: UserAddress = {
      province: null,
      ward: null,
      detail: null,
    };
    const tutor: Tutor =
      data.role === USER_ROLES.TUTOR
        ? {
            job: null,
            description: null,
          }
        : null;
    const user: User = {
      address,
      tutor,
      username: data.username,
      password: await hashPassword(data.password),
      authorizated: socialAuth,
      isNew: true,
      birthday: null,
      role: get(data, 'role', USER_ROLES.FINDER),
      email: get(data, 'email', null),
      phone: null,
      displayName: get(data, 'displayName', null),
      gender: GENDERS.OTHER,
      lock: false,
      facebookId: get(data, 'facebookId', null),
      googleId: get(data, 'googleId', null),
    };

    const res: User = await this.repository.save(user);

    return res ? res.id : null;
  }

  async sendAuthorizationEmail(userId: number): Promise<string> {
    this.getRepository();
    const user: User = await this.repository.findOne({
      where: { id: userId },
      select: ['id', 'email'],
    });
    const token = this.createAuthorizationToken(user);

    const html = `<div style="width: 100%;">
        <div style="width: 100%; padding: 20px 0px; text-align: center; font-size: 20px; font-weight: bold; color: #1aa3ff;">Kích hoạt tài khoản</div>
        <div>
          My tutor chào mừng bạn đến với hệ thống kết nối gia sư trực tuyến - củng cố con đường học vấn của bạn.<br />
          Cảm ơn bạn đã tin tưởng và đăng ký tài khoản MyTutor của chúng tôi, bạn vui lòng click vào button bên dưới để tiến hành kích hoạt tài khoản nhé:<br />
        </div>
        <a
          style="display: inline-block; background: #1aa3ff; color: #fff; padding: 10px 20px; text-decoration: none; margin: 20px auto;" 
          href="${commonConfigs.AUTHORIZATION_ACCOUNT_HOST}/auth/authorizate-account/${token}">
          Kích hoạt tài khoản tại đây
        </a>
      </div>`;

    await mailService
      .mail(user.email, 'MyTutor - Kích hoạt tài khoản', html)
      .then(info => console.log('mail info', info))
      .catch(err => console.log('send authorization email error', err));
    await this.repository.update({ id: userId }, { authorizationToken: token });

    return token;
  }

  async authorizateAccount(id) {
    this.getRepository();
    this.repository.update({ id }, { authorizated: true });
  }

  async isValidAuthorizationToken(token: string): Promise<boolean> {
    this.getRepository();
    try {
      const user = await this.repository.findOne({
        where: { authorizationToken: token },
        select: ['id'],
      });
      console.log('user', token, user);

      if (user) {
        await this.authorizateAccount(user.id);
        return true;
      }

      return false;
    } catch (err) {
      console.log('err', err);
      return false;
    }
  }

  async getProfile(id: number) {
    this.getRepository();
    return this.repository.findOne({
      where: { id },
      select: ['id', 'displayName', 'gender', 'birthday', 'email', 'phone', 'avatar', 'role'],
      relations: ['address'],
    });
  }

  @Transaction({ isolation: 'SERIALIZABLE' })
  async updateProfile(
    id: number,
    data: any,
    @TransactionRepository(User) tUserRepository?: Repository<User>,
    @TransactionRepository(UserAddress) tUserAddressRepository?: Repository<UserAddress>,
  ): Promise<User> {
    console.log('update profile data: ', data);
    let user = await this.findOne({ where: { id }, loadRelationIds: { relations: ['address'] } });
    let address = await tUserAddressRepository.findOne({
      where: { id: user.address },
      select: ['id', 'province', 'ward', 'detail'],
    });

    if (data.province || data.ward || data.detail) {
      const addressData = removeUndefinedField({
        province: get(data, 'province'),
        ward: get(data, 'ward'),
        detail: get(data, 'detail'),
      });
      console.log('addressData: ', addressData);

      await tUserAddressRepository.update({ id: address.id }, addressData);
      address = await tUserAddressRepository.findOne({
        where: { id: address.id },
        select: ['id', 'province', 'ward', 'detail'],
      });
    }

    const profile = {
      ...omit(data, ['province', 'ward', 'detail']),
      isNew: false,
    };
    console.log('profile', profile);
    console.log('----------------------');
    await tUserRepository.update({ id }, profile);

    user = {
      ...(await tUserRepository.findOne({
        where: { id },
        select: ['id', 'displayName', 'gender', 'birthday', 'email', 'phone', 'avatar', 'role'],
      })),
      address,
    };

    return user;
  }

  async getTutorInfo(id: number): Promise<User> {
    return this.findOne({
      where: { id },
      select: ['id', 'role'],
      relations: ['tutor', 'skills'],
    });
  }

  async updateTutorInfo(userId: number, data: any): Promise<User> {
    const user = await this.findOne({
      where: { id: userId },
      select: ['id'],
      relations: ['tutor'],
    });

    if (data.skills) {
      user.skills = data.skills.map(skillId => ({ id: skillId }));
    }

    hasIn(data, 'tuitionPerHour') && (user.tutor.tuitionPerHour = data.tuitionPerHour);
    hasIn(data, 'description') && (user.tutor.description = data.description);
    hasIn(data, 'job') && (user.tutor.job = data.job);

    await this.repository.save(user);

    return this.findOne({
      where: { id: user.id },
      select: ['id', 'role'],
      relations: ['skills', 'tutor'],
    });
  }

  async getTutors(
    province: string,
    ward: string,
    skills: string[] = [],
    minTuition: number = 0,
    maxTuition: number = -1,
    page: number = 1,
    pageSize: number = 12,
    increaseTuition: boolean = true,
  ): Promise<User[]> {
    console.log('skills', skills);
    this.getRepository();
    const limit = pageSize;
    const offset = (page - 1) * limit;
    let query = this.repository
      .createQueryBuilder('user')
      .select('user.id')
      // .addSelect('user.displayName')
      // .addSelect('user.avatar')
      // .addSelect('user.role')
      .innerJoinAndSelect('user.tutor', 'tutor')
      .leftJoin('user.address', 'address')
      .leftJoin('user.skills', 'skills')
      .where('user.id')
      .andWhere('user.delete=0')
      .andWhere('user.lock=0')
      .andWhere('user.authorizated=1');

    if (province) {
      query = query.andWhere('address.province=:province', { province });

      if (ward) {
        query = query.andWhere('address.ward=:ward', { ward });
      }
    }

    query = query.andWhere('tutor.tuitionPerHour>=:minTuition', { minTuition });

    if (maxTuition && maxTuition > -1) {
      query = query.andWhere('tutor.tuitionPerHour<=:maxTuition', { maxTuition });
    }

    if (skills.length > 0) {
      query = query.andWhere(`skills.id in(${[...skills]})`);
    }

    query = query
      .limit(limit)
      .offset(offset)
      .orderBy('tutor.tuitionPerHour', increaseTuition ? 'ASC' : 'DESC');

    return query.getMany().then(users => {
      if (users.length === 0) {
        return [];
      }

      return this.find({
        where: { id: In(users.map(u => u.id)) },
        select: ['id', 'displayName', 'avatar', 'role'],
        relations: ['address', 'tutor', 'skills'],
      }).then(users =>
        users.sort((first, second) =>
          increaseTuition
            ? first.tutor.tuitionPerHour - second.tutor.tuitionPerHour
            : second.tutor.tuitionPerHour - first.tutor.tuitionPerHour,
        ),
      );
    });
  }

  async getTutor(userId: number): Promise<User> {
    if (!userId) {
      return null;
    }

    return this.findOne({
      where: { id: userId, delete: false },
      select: ['id', 'displayName', 'email', 'gender', 'phone', 'avatar'],
      relations: ['skills', 'address', 'tutor'],
    });
  }

  async getHighRateTutors(): Promise<User[]> {
    console.log('get 3');
    const tutors = await tutorService.find({
      where: { delete: false },
      select: ['id'],
      take: 3,
      skip: 0,
      order: {
        rate: 'DESC',
      },
    });

    return this.find({
      where: {
        tutor: { id: In(tutors.map(tutor => tutor.id)) },
        lock: false,
        authorizated: true,
      },
      select: ['id', 'displayName', 'avatar', 'gender'],
      relations: ['tutor', 'address', 'skills'],
    });
  }

  async getContracts(
    userId: number,
    page = 1,
    pageSize = commonConfigs.PAGE_SIZE,
    contractStatus = -1,
  ): Promise<{ page: number; totalPage: number; contracts: Contract[] }> {
    const take = pageSize;
    const skip = (page - 1) * take;
    const user = await this.findOne({
      where: { id: userId },
      select: ['id', 'role'],
    });
    const totalCount = await contractService.count({
      where: {
        ...(user.role === USER_ROLES.TUTOR
          ? { tutor: { id: userId } }
          : { finder: { id: userId } }),
        ...(contractStatus > -1 ? { status: contractStatus } : {}),
        delete: false,
      },
    });
    console.log('totalcount', totalCount);
    const totalPage = Math.ceil((totalCount * 1.0) / take);

    if (page > totalPage) {
      return {
        page,
        totalPage,
        contracts: [],
      };
    }

    const contracts = await contractService.find({
      where: {
        ...(user.role === USER_ROLES.TUTOR
          ? { tutor: { id: userId } }
          : { finder: { id: userId } }),
        ...(contractStatus > -1 ? { status: contractStatus } : {}),
        delete: false,
      },
      select: CONTRACT_FIELDS_NEED_TO_GET,
      relations: ['tutor', 'finder'],
      take,
      skip,
      order: {
        createdDate: 'DESC',
      },
    });

    return {
      page,
      totalPage,
      contracts: contracts.map(contract => ({
        ...contract,
        tutor: omit(contract.tutor, USER_FIELDS_NEED_TO_REMOVE),
        finder: omit(contract.finder, USER_FIELDS_NEED_TO_REMOVE),
      })),
    };
  }

  async addNewSkill(userId: number, skillTitle: string): Promise<Skill> {
    const user = await this.findOne({
      where: { id: userId },
      select: ['id'],
      relations: ['skills'],
    });
    const skill = (await skillService.save([{ title: skillTitle }]))[0];

    user.skills = [...user.skills, skill];

    await this.save([user]);

    return omit(skill, ['delete']);
  }

  async isTrustPassword(userId: number, oldPassword: string): Promise<boolean> {
    const user = await this.findOne({
      where: {
        id: userId,
      },
      select: ['id', 'password'],
    });

    if (bcrypt.compareSync(oldPassword, user.password)) {
      return true;
    }

    return false;
  }

  async changePassword(userId: number, newPassword: string): Promise<boolean> {
    const user = await this.update({ id: userId }, { password: await hashPassword(newPassword) }, [
      'id',
    ]);

    return user ? true : false;
  }

  async sendResetPasswordEmail(email: string): Promise<string> {
    const user = await this.findOne({
      where: { email, lock: false, delete: false },
      select: ['id', 'username', 'email', 'role'],
    });
    const token = this.createAuthorizationToken(Object.assign({}, user));

    const html = `<div style="width: 100%;">
        <div style="width: 100%; padding: 20px 0px; text-align: center; font-size: 20px; font-weight: bold; color: #1aa3ff;">Khôi phục mật khẩu</div>
        <div>
          My tutor chào mừng bạn đến với hệ thống kết nối gia sư trực tuyến - củng cố con đường học vấn của bạn.<br />
          Chúng tôi rất tiếc khi bạn đã quên mất mật khẩu của mình. Nhưng không sao, bạn hãy click vào button bên dưới để tiến hành tạo mới mật khẩu để có thể tiếp tục nhé:<br />
        </div>
        <a
          style="display: inline-block; background: #1aa3ff; color: #fff; padding: 10px 20px; text-decoration: none; margin: 20px auto;" 
          href="https://mytutor-advance.herokuapp.com/reset-password?token=${token}">
          Khôi phục mật khẩu tại đây
        </a>
      </div>`;

    await mailService
      .mail(email, 'MyTutor - Khôi phục mật khẩu', html)
      .then(info => console.log('mail info', info))
      .catch(err => console.log('send authorization email error', err));
    await this.repository.update({ id: user.id }, { authorizationToken: token });

    return token;
  }
}

export const userService = new UserService();
