import {
  Repository,
  getConnection,
  EntitySchema,
  ObjectType,
  FindOneOptions,
  FindConditions,
  FindManyOptions,
  In,
} from 'typeorm';
import { keysIn, get, omit } from 'lodash';
import { QueryDeepPartialEntity } from 'typeorm/query-builder/QueryPartialEntity';

export class BaseService<T> {
  repository: Repository<T>;
  RepositoryDefinition: ObjectType<T>;

  constructor(Repo: ObjectType<T>) {
    this.repository = null;
    this.RepositoryDefinition = Repo;
  }

  getRepository() {
    !this.repository &&
      (this.repository = getConnection().getRepository(this.RepositoryDefinition));
  }

  async findOne(options: FindOneOptions<T>): Promise<T> {
    this.getRepository();
    return this.repository.findOne(options);
  }

  async find(options: FindManyOptions<T>): Promise<T[]> {
    this.getRepository();
    return this.repository.find(options);
  }

  async findManyByPrimaryKeys(
    key: string,
    keyVals: any[],
    options: FindManyOptions<T>,
  ): Promise<T[]> {
    this.getRepository();
    return this.repository.find({
      where: {
        [key]: In(keyVals),
        ...get(options, 'where', {}),
        ...omit(options, ['where']),
      },
    });
  }

  async isExists(conditions: FindConditions<T>, fieldtoCheck: string): Promise<boolean> {
    const instance = await this.findOne({
      where: conditions,
      select: fieldtoCheck ? [fieldtoCheck] : keysIn(conditions),
    }).catch(err => console.log('BaseService:isExists:error: ', err));

    return instance ? true : false;
  }

  async update(
    where: FindConditions<T>,
    data: QueryDeepPartialEntity<T>,
    returnFields = [],
    returnRelations = [],
  ): Promise<T> {
    this.getRepository();
    await this.repository.update(where, data);

    return this.findOne({ where, select: returnFields, relations: returnRelations });
  }

  async count(options: FindManyOptions<T>): Promise<number> {
    this.getRepository();
    return this.repository.count(options);
  }

  async save(entities: T[]): Promise<T[]> {
    this.getRepository();
    return this.repository.save(entities);
  }
}
