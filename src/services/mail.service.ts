import * as nodemailer from 'nodemailer';
const { google } = require('googleapis');
import Mail = require('nodemailer/lib/mailer');
import { MailOptions } from 'nodemailer/lib/json-transport';
import { googleConfigs } from '../commons/configs/google.config';

const OAuth2 = google.auth.OAuth2;

class MailService {
  transporter: Mail;
  from: string;
  oauth2Client;

  constructor() {
    this.from = 'tunhbd1998@gmail.com';
    this.initOauth2Client();
  }

  private initOauth2Client() {
    this.oauth2Client = new OAuth2(
      googleConfigs.CLIENT_ID,
      googleConfigs.CLIENT_SECRET,
      'https://developers.google.com/oauthplayground',
    );
    this.oauth2Client.setCredentials({
      refresh_token: googleConfigs.REFRESH_TOKEN,
    });
  }

  private async createTransporter() {
    const accessToken = await this.oauth2Client
      .getAccessToken()
      .catch(err => console.log('get access token error', err));
    // console.log('access token', accessToken);
    this.transporter = nodemailer.createTransport({
      service: 'gmail',
      auth: {
        type: 'OAuth2',
        user: 'tunhbd1998@gmail.com',
        clientId: googleConfigs.CLIENT_ID,
        clientSecret: googleConfigs.CLIENT_SECRET,
        refreshToken: googleConfigs.REFRESH_TOKEN,
        accessToken: accessToken,
      },
    });
  }

  private createMailOptions(email, subject, html): MailOptions {
    return {
      from: this.from,
      to: email,
      subject,
      html,
    };
  }

  async mail(email: string, subject: string, html: string): Promise<any> {
    await this.createTransporter();
    return this.transporter.sendMail(this.createMailOptions(email, subject, html));
  }
}

export const mailService = new MailService();
