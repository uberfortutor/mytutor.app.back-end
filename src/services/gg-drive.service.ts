import {
  GoogleDriveClient,
  GOOGLE_DRIVE_PERMISSION_TYPE,
  GOOGLE_DRIVE_PERMISSION_ROLE,
} from 'google-drive-client/build/src';
import { join } from 'path';
import { isEmpty } from 'lodash';
import { UploadFileResponse } from 'google-drive-client/src/defines/responses';

class GoogleDriveService {
  client: GoogleDriveClient;

  constructor() {
    this.client = new GoogleDriveClient({
      tokenPath: join(__dirname + '/../../token.json'),
      credentialsPath: join(__dirname, '/../../credentials.json'),
      scopes: [
        'https://www.googleapis.com/auth/drive',
        'https://www.googleapis.com/auth/drive.file',
        'https://www.googleapis.com/auth/drive.appfolder',
      ],
    });
    console.log(join(__dirname + '/../../token.json'));
    console.log(join(__dirname, '/../../credentials.json'));
  }

  async uploadFile(localPath, mimeType): Promise<UploadFileResponse> {
    if (isEmpty(localPath)) {
      return null;
    }
    const splits = localPath.split('/');
    const filename = splits[splits.length - 1];

    return this.client.uploadFile({
      filename,
      mimeType,
      toFolder: 'mytutor-user-avatars',
      folderId: null,
      fileUrl: localPath,
      permissions: [
        {
          type: GOOGLE_DRIVE_PERMISSION_TYPE.ANYONE,
          role: GOOGLE_DRIVE_PERMISSION_ROLE.READER,
        },
      ],
    });
  }

  async deleteFile(fileId: string) {
    this.client.deleteFile(fileId);
  }
}

export const googleDriveService = new GoogleDriveService();
