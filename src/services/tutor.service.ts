import { BaseService } from './base.service';
import { Tutor } from '../database/entities/tutor.entity';
import { User } from '../database/entities/user.entity';
import { userService } from './user.service';
import { In, Between } from 'typeorm';

class TutorService extends BaseService<Tutor> {
  constructor() {
    super(Tutor);
  }
}

export const tutorService = new TutorService();
