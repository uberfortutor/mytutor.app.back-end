import { BaseService } from './base.service';
import { Notification } from '../database/entities/notification.entity';

class NotificationService extends BaseService<Notification> {
  constructor() {
    super(Notification);
  }
}

export const notificationService = new NotificationService();
