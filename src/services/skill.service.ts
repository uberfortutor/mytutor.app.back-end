import { BaseService } from './base.service';
import { Skill } from '../database/entities/skill.entity';

class SkillService extends BaseService<Skill> {
  constructor() {
    super(Skill);
  }
}

export const skillService = new SkillService();
