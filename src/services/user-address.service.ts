import { BaseService } from './base.service';
import { UserAddress } from '../database/entities/user-address.entity';

class UserAddressService extends BaseService<UserAddress> {
  constructor() {
    super(UserAddress);
  }
}

export const userAddressService = new UserAddressService();
