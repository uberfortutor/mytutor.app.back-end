import { pick } from 'lodash';
import { BaseService } from './base.service';
import { TutorEvaluation } from '../database/entities/tutor-evaluation.entity';
import { commonConfigs } from '../commons/configs';
import { userService } from './user.service';

class TutorEvaluationService extends BaseService<TutorEvaluation> {
  constructor() {
    super(TutorEvaluation);
  }

  async getEvaluations(
    userId: number,
    page: number = 1,
    pageSize: number = commonConfigs.PAGE_SIZE,
  ): Promise<TutorEvaluation[]> {
    return this.find({
      where: {
        tutor: {
          id: userId,
        },
        delete: false,
      },
      select: ['id', 'rate', 'content', 'createdDate'],
      relations: ['tutor', 'finder'],
      take: pageSize,
      skip: (page - 1) * pageSize,
      order: { createdDate: 'DESC', id: 'DESC' },
    }).then((evaluations: TutorEvaluation[]) =>
      evaluations.map(val => ({
        ...val,
        tutor: pick(val.tutor, ['id', 'displayName', 'avatar']),
        finder: pick(val.finder, ['id', 'displayName', 'avatar']),
      })),
    );
  }

  async createEvaluation(
    finderId: number,
    tutorId: number,
    content: string,
    rate: number,
  ): Promise<{ status: boolean; evaluation: TutorEvaluation }> {
    this.getRepository();
    if (await userService.isExists({ id: tutorId, delete: false }, 'id')) {
      const evaluation = (
        await this.save([
          {
            content,
            rate,
            finder: { id: finderId },
            tutor: { id: tutorId },
          },
        ])
      )[0];

      evaluation.tutor = await userService.findOne({
        where: { id: evaluation.tutor.id },
        select: ['id', 'displayName', 'avatar'],
      });

      evaluation.finder = await userService.findOne({
        where: { id: evaluation.finder.id },
        select: ['id', 'displayName', 'avatar'],
      });

      return { status: true, evaluation };
    }

    return { status: false, evaluation: null };
  }
}

export const tutorEvaluationService = new TutorEvaluationService();
