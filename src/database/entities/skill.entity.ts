import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity('skill')
export class Skill {
  @PrimaryGeneratedColumn()
  id?: number;

  @Column({ type: 'text', default: null })
  title?: string;

  @Column({ type: 'boolean', default: false })
  delete?: boolean;
}
