import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  JoinColumn,
  CreateDateColumn,
} from 'typeorm';
import { Tutor } from './tutor.entity';
import { User } from './user.entity';

@Entity('tutor_evaluation')
export class TutorEvaluation {
  @PrimaryGeneratedColumn()
  id?: number;

  @Column({ type: 'float', default: 0 })
  rate?: number;

  @Column({ type: 'text', default: null })
  content?: string;

  @Column({ type: 'boolean', default: false })
  delete?: boolean;

  @CreateDateColumn()
  createdDate?: Date;

  @ManyToOne(
    type => User,
    tutor => tutor.evaluationsForMe,
    {
      onDelete: 'CASCADE',
    },
  )
  @JoinColumn()
  tutor?: Tutor;

  @ManyToOne(
    type => User,
    finder => finder.myEvaluations,
    { onDelete: 'CASCADE' },
  )
  @JoinColumn()
  finder?: User;
}
