import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  JoinColumn,
  OneToMany,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Tutor } from './tutor.entity';
import { User } from './user.entity';
import { ContractComplain } from './contract-complain.entity';
import { Payment } from './payment.entity';

@Entity('contract')
export class Contract {
  @PrimaryGeneratedColumn()
  id?: number;

  @Column({ type: 'text', default: null })
  name?: string;

  @Column({ type: 'text', default: null })
  teach?: string;

  @Column({ type: 'tinyint', default: 0 })
  status?: number;

  @Column({ type: 'int', default: 0 })
  numberOfSession?: number;

  @Column({ type: 'tinyint', default: 0 })
  numberOfHourPerSession?: number;

  @Column({ type: 'float', default: 0 })
  tuitionOfFinder?: number;

  @Column({ type: 'float', default: 0 })
  confirmedTuition?: number;

  @Column({ type: 'boolean', default: false })
  signRequestOfFinder?: boolean;

  @Column({ type: 'boolean', default: false })
  signRequestOfTutor?: boolean;

  @CreateDateColumn()
  createdDate?: Date;

  @UpdateDateColumn()
  updatedDate?: Date;

  @Column({ type: 'boolean', default: false })
  delete?: boolean;

  @ManyToOne(
    type => User,
    tutor => tutor.receivedContracts,
    { onDelete: 'CASCADE' },
  )
  @JoinColumn()
  tutor?: User;

  @ManyToOne(
    type => User,
    finder => finder.requestedContracts,
    { onDelete: 'CASCADE' },
  )
  @JoinColumn()
  finder?: User;

  @OneToMany(
    type => ContractComplain,
    contractComplain => contractComplain.contract,
  )
  complains?: ContractComplain[];

  @OneToMany(
    type => Payment,
    payment => payment.contract,
  )
  payments?: Payment[];
}
