import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn } from 'typeorm';
import { User } from './user.entity';
import { Contract } from './contract.entity';

@Entity('contract_complain')
export class ContractComplain {
  @PrimaryGeneratedColumn()
  id?: number;

  @Column({ type: 'text', default: null })
  content?: string;

  @Column({
    type: 'boolean',
    default: false,
  })
  status?: boolean;

  @Column({ type: 'boolean', default: false })
  delete?: boolean;

  @ManyToOne(
    type => User,
    finder => finder.requestedComplains,
  )
  @JoinColumn()
  finder?: User;

  @ManyToOne(
    type => Contract,
    contract => contract.complains,
  )
  contract?: Contract;
}
