import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToOne,
  OneToMany,
  ManyToMany,
  JoinTable,
  JoinColumn,
} from 'typeorm';
import { CONNREFUSED } from 'dns';
import { UserAddress } from './user-address.entity';
import { TutorEvaluation } from './tutor-evaluation.entity';
import { Tutor } from './tutor.entity';
import { USER_ROLES } from '../../commons/defines/enums/user-role.enum';
import { GENDERS } from '../../commons/defines/enums/gender.enum';
import { Skill } from './skill.entity';
import { Contract } from './contract.entity';
import { ContractComplain } from './contract-complain.entity';
import { Payment } from './payment.entity';
import { Message } from './message.entity';
import { Notification } from './notification.entity';

@Entity('user')
export class User {
  @PrimaryGeneratedColumn()
  id?: number;

  @Column({ type: 'varchar', unique: true, default: null })
  username?: string;

  @Column({ type: 'varchar', default: null })
  password?: string;

  @Column({ type: 'varchar', default: null })
  avatar?: string;

  @Column({ type: 'varchar', default: null })
  avatarId?: string;

  @Column({ type: 'varchar', default: null })
  facebookId?: string;

  @Column({ type: 'varchar', default: null })
  googleId?: string;

  @Column({ type: 'text', default: null })
  displayName?: string;

  @Column({ type: 'varchar', default: null })
  email?: string;

  @Column({ type: 'varchar', default: null })
  phone?: string;

  @Column({ type: 'date', default: null })
  birthday?: Date;

  @Column({ type: 'enum', enum: GENDERS, default: GENDERS.MALE })
  gender?: GENDERS;

  @Column({ type: 'enum', enum: USER_ROLES, default: USER_ROLES.FINDER })
  role?: USER_ROLES;

  @Column({ type: 'boolean', default: false })
  lock?: boolean;

  @Column({ type: 'boolean', default: false })
  authorizated?: boolean;

  @Column({ type: 'varchar', default: null })
  authorizationToken?: string;

  @Column({ type: 'boolean', default: true })
  isNew?: boolean;

  @OneToOne(type => UserAddress, { cascade: true })
  @JoinColumn()
  address?: UserAddress;

  @OneToOne(type => Tutor, { cascade: true })
  @JoinColumn()
  tutor?: Tutor;

  @Column({ type: 'boolean', default: false })
  delete?: boolean;

  @OneToMany(
    type => TutorEvaluation,
    evaluation => evaluation.finder,
    {
      cascade: true,
    },
  )
  myEvaluations?: TutorEvaluation[];

  @OneToMany(
    type => TutorEvaluation,
    evaluation => evaluation.tutor,
  )
  evaluationsForMe?: TutorEvaluation[];

  @ManyToMany(type => Skill)
  @JoinTable({
    name: 'tutor_skill',
    joinColumn: { name: 'tutorId' },
    inverseJoinColumn: { name: 'skillId' },
  })
  skills?: Skill[];

  @OneToMany(
    type => Contract,
    contract => contract.finder,
    { cascade: true },
  )
  requestedContracts?: Contract[];

  @OneToMany(
    type => Contract,
    contract => contract.tutor,
    { cascade: true },
  )
  receivedContracts?: Contract[];

  @OneToMany(
    type => ContractComplain,
    contractComplain => contractComplain.finder,
  )
  requestedComplains?: ContractComplain[];

  @OneToMany(
    type => Payment,
    payment => payment.tutor,
  )
  receivedPayments?: Payment[];

  @OneToMany(
    type => Payment,
    payment => payment.finder,
  )
  sentPayments?: Payment[];

  @OneToMany(
    type => Message,
    message => message.receiver,
  )
  receivedMessages?: Message[];

  @OneToMany(
    type => Message,
    message => message.sender,
  )
  sentMessages?: Message[];

  @OneToMany(
    type => Notification,
    notification => notification.for,
  )
  notifications?: Notification[];
}
