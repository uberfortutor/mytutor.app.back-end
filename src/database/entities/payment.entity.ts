import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn } from 'typeorm';
import { Contract } from './contract.entity';
import { User } from './user.entity';

@Entity('payment')
export class Payment {
  @PrimaryGeneratedColumn()
  id?: number;

  @Column({ type: 'date', default: null })
  paidDate?: Date;

  @Column({ type: 'double', default: 0 })
  amount?: number;

  @Column({ type: 'boolean', default: false })
  delete?: boolean;

  @ManyToOne(
    type => Contract,
    contract => contract.payments,
  )
  @JoinColumn()
  contract?: Contract;

  @ManyToOne(
    type => User,
    tutor => tutor.receivedPayments,
  )
  @JoinColumn()
  tutor?: User;

  @ManyToOne(
    type => User,
    finder => finder.sentPayments,
  )
  @JoinColumn()
  finder?: User;
}
