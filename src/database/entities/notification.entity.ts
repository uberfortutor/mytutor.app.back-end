import {
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  JoinColumn,
  Entity,
  CreateDateColumn,
} from 'typeorm';
import { NOTIFICATION_TYPE } from '../../commons/defines/enums';
import { User } from './user.entity';

@Entity('notification')
export class Notification {
  @PrimaryGeneratedColumn()
  id?: number;

  @Column({ type: 'enum', enum: NOTIFICATION_TYPE, nullable: false })
  type?: NOTIFICATION_TYPE;

  @CreateDateColumn()
  createdDate?: Date;

  @Column({ type: 'boolean', default: false })
  read?: boolean;

  @Column({ type: 'json', default: null })
  metadata?: object;

  @ManyToOne(
    type => User,
    user => user.notifications,
    { nullable: false },
  )
  @JoinColumn()
  for?: User;
}
