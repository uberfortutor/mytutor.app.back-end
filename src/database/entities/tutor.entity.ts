import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity('tutor')
export class Tutor {
  @PrimaryGeneratedColumn()
  id?: number;

  @Column({ type: 'text', default: null })
  job?: string;

  @Column({ type: 'longtext', default: null })
  description?: string;

  @Column({ type: 'float', default: 0 })
  tuitionPerHour?: number;

  @Column({ type: 'double', default: 0 })
  wallet?: number;

  @Column({ type: 'float', default: 0 })
  rate?: number;

  @Column({ type: 'int', default: 0 })
  numberOfEvaluation?: number;

  @Column({ type: 'boolean', default: false })
  delete?: boolean;
}
