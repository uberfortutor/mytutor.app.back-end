import {
  Entity,
  OneToOne,
  PrimaryColumn,
  JoinColumn,
  Column,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { User } from './user.entity';
import { PROVINCES } from '../../commons/defines/enums/province.enum';

@Entity('user_address')
export class UserAddress {
  @PrimaryGeneratedColumn()
  id?: number;

  @Column({ type: 'varchar', default: null })
  province?: string;

  @Column({ type: 'text', default: null })
  ward?: string;

  @Column({ type: 'text', default: null })
  detail?: string;

  @Column({ type: 'boolean', default: false })
  delete?: boolean;
}
