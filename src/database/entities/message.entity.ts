import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  ManyToOne,
  JoinColumn,
} from 'typeorm';
import { User } from './user.entity';

@Entity('message')
export class Message {
  @PrimaryGeneratedColumn()
  id?: number;

  @Column({ type: 'text', default: null })
  content?: string;

  @CreateDateColumn()
  sentDate?: Date;

  @Column({ type: 'boolean', default: false })
  delete?: boolean;

  @ManyToOne(
    type => User,
    receiver => receiver.receivedMessages,
  )
  @JoinColumn()
  receiver?: User;

  @ManyToOne(
    type => User,
    sender => sender.sentMessages,
  )
  @JoinColumn()
  sender?: User;
}
