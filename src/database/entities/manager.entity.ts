import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';
import { MANAGER_ROLE } from '../../commons/defines/enums/manager-role.enum';

@Entity('manager')
export class Manager {
  @PrimaryGeneratedColumn()
  id?: number;

  @Column({ type: 'varchar', default: null })
  username?: string;

  @Column({ type: 'varchar', default: null })
  password?: string;

  @Column({ type: 'text', default: null })
  name?: string;

  @Column({ type: 'tinyint', default: 1 })
  role?: number;

  @Column({ type: 'varchar', default: null })
  email?: string;

  @Column({ type: 'tinyint', default: 0 })
  lock?: boolean;

  @Column({ type: 'boolean', default: false })
  delete?: boolean;
}
