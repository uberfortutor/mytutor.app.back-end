import { createConnection, ConnectionOptions, Connection } from 'typeorm';
import { join } from 'path';

const connectionOptions: ConnectionOptions = {
  type: 'mysql',
  host: '34.87.108.23',
  port: 3307,
  username: 'root',
  password: 'root1234567',
  database: 'mytutor',
  synchronize: false,
  logging: true,
  entities: [join(__dirname, './entities/**.entity{.ts,.js}')],
};

export const connectDatabase = async () => {
  return await createConnection(connectionOptions)
    .then(() => {
      console.log('Connect to database successfully');
    })
    .catch(err => console.log('connect to database error: ', err));
};

// const createConnection = () => {
//   return mysql.createConnection({
//     host: '34.87.108.23',
//     port: '3307',
//     user: 'root',
//     password: 'root1234567',
//     database: 'mytutor',
//   });
// };

// module.exports = {
//   load: sql => {
//     return new Promise((resolve, reject) => {
//       const connection = createConnection();
//       connection.query(sql, (err, result, fields) => {
//         if (err) reject(err);
//         else {
//           resolve(result);
//         }
//         connection.end();
//       });
//     });
//   },
//   add: (tableName, entity) => {
//     return new Promise((resolve, reject) => {
//       const connection = createConnection();
//       const sql = `insert into ${tableName} set ?`;
//       connection.query(sql, entity, (err, value) => {
//         if (err) reject(err);
//         else {
//           resolve(value.insertId);
//         }
//         connection.end();
//       });
//     });
//   },
//   delete: (tableName, idField, id) => {
//     return new Promise((resolve, reject) => {
//       const connection = createConnection();
//       const sql = `delete from ${tableName} where ${idField} = ?`;
//       connection.query(sql, id, (err, value) => {
//         if (err) reject(err);
//         else {
//           resolve(value);
//         }
//         connection.end();
//       });
//     });
//   },
//   deleteWhenHavePrimaryFields: (tableName, idFields, ids) => {
//     return new Promise((resolve, reject) => {
//       const connection = createConnection();
//       let sql = `delete from ${tableName} where `;
//       const isFirst = true;
//       for (let i = 0; i < idFields.length; i++) {
//         sql += idFields[i] + ' = ? ';
//         if (i != idFields.length - 1) {
//           sql += ' and ';
//         }
//       }
//       connection.query(sql, ids, (err, value) => {
//         if (err) reject(err);
//         else {
//           resolve(value);
//         }
//         connection.end();
//       });
//     });
//   },
//   update: (tableName, entity, idField) => {
//     return new Promise((resolve, reject) => {
//       const connection = createConnection();
//       const id = entity[idField];
//       delete entity[idField];
//       const sql = `update ${tableName} set ? where ${idField} = ?`;
//       connection.query(sql, [entity, id], (err, value) => {
//         if (err) reject(err);
//         else {
//           resolve(value);
//         }
//         connection.end();
//       });
//     });
//   },
//   updateWhenHavePrimaryFields: (tableName, entity, idFields) => {
//     return new Promise((resolve, reject) => {
//       const connection = createConnection();
//       let sql = `update ${tableName} set ? where `;
//       const primakeyFieldsValue = [];
//       for (i = 0; i < idFields.length; i++) {
//         if (i != 0) sql += ' and ';
//         sql += `${idFields[i]} = ? `;
//         primakeyFieldsValue.push(entity[idFields[i]]);
//         delete entity[idFields[i]];
//       }

//       const params = [entity];
//       for (i = 0; i < idFields.length; i++) {
//         params.push(primakeyFieldsValue[i]);
//       }

//       connection.query(sql, params, (err, value) => {
//         if (err) reject(err);
//         else {
//           resolve(value);
//         }
//         connection.end();
//       });
//     });
//   },
// };
