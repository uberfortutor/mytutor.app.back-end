import * as bcrypt from 'bcrypt';
import { bcryptConfig } from '../commons/configs/bcrypt.config';

export const hashPassword = async plainPassword =>
  await bcrypt.hash(plainPassword, bcryptConfig.SALT_ROUNDS);
