import { pick, keysIn } from 'lodash';

export const removeUndefinedField = obj => {
  return pick(
    obj,
    keysIn(obj).filter(key => obj[key] !== undefined),
  );
};
