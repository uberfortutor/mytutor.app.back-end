import { Expose } from 'class-transformer';
import { IsOptional, IsNumber } from 'class-validator';

export class UpdateContractTutorDTO {
  @Expose()
  @IsOptional()
  @IsNumber()
  tuition?: number;

  @Expose()
  @IsOptional()
  @IsNumber()
  numberOfSession?: number;

  @Expose()
  @IsOptional()
  @IsNumber()
  numberOfHourPerSession?: number;
}
