import { IsNotEmpty, IsString, IsNumber } from 'class-validator';
import { Expose } from 'class-transformer';

export class CreateContractDTO {
  @Expose()
  @IsNotEmpty()
  @IsString()
  name?: string;

  @Expose()
  @IsNotEmpty()
  @IsString()
  teach?: string;

  @Expose()
  @IsNotEmpty()
  @IsNumber()
  tuition?: number;

  @Expose()
  @IsNotEmpty()
  @IsNumber()
  tutorId?: number;
}
