import { IsOptional, IsNotIn, IsNotEmpty, IsString, IsNumber, IsArray } from 'class-validator';
import { Expose } from 'class-transformer';

export class UpdateTutorInfoDTO {
  @Expose()
  @IsOptional()
  @IsString()
  description?: string;

  @Expose()
  @IsOptional()
  @IsNumber()
  tuitionPerHour?: number;

  @Expose()
  @IsOptional()
  @IsString()
  job?: string;

  @Expose()
  @IsOptional()
  @IsArray()
  skills?: string[];
}
