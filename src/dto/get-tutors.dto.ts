import {
  IsString,
  IsOptional,
  IsEnum,
  IsNumberString,
  Min,
  IsNumber,
  IsBooleanString,
} from 'class-validator';
import { PROVINCES } from '../commons/defines/enums';
import { Expose } from 'class-transformer';
import { MinNumberString } from '../commons/custom-validator-decorators/min-number-string.validator-decorator';

export class GetTutorsDTO {
  @Expose()
  @IsOptional()
  @IsString()
  province?: string;

  @Expose()
  @IsOptional()
  @IsString()
  ward?: string;

  @Expose()
  @IsOptional()
  @MinNumberString(0, { message: 'Must not less than 0' })
  @IsNumberString()
  minTuition?: string;

  @Expose()
  @IsOptional()
  @MinNumberString(0, { message: 'Must not less than 0' })
  @IsNumberString()
  maxTuition?: string;

  @Expose()
  @IsOptional()
  @MinNumberString(1, { message: 'Must not less than 1' })
  @IsNumberString()
  page?: string;

  @Expose()
  @IsOptional()
  @MinNumberString(1, { message: 'Must not less than 1' })
  @IsNumberString()
  pageSize?: string;

  @Expose()
  @IsOptional()
  @IsString()
  skills?: string;

  @Expose()
  @IsOptional()
  @IsBooleanString()
  increaseTuition?: string;
}
