import { IsNotEmpty, IsString } from 'class-validator';
import { Expose } from 'class-transformer';

export class CheckExistsUsernameDTO {
  @Expose()
  @IsNotEmpty()
  @IsString()
  username: string;
}
