import { GENDERS } from '../commons/defines/enums/gender.enum';
import { USER_ROLES } from '../commons/defines/enums/user-role.enum';
import { UserAddress } from '../database/entities/user-address.entity';
import { IsString, IsOptional, IsEmail, IsEnum, IsNotEmpty } from 'class-validator';
import { Expose } from 'class-transformer';

export class SignUpUserDTO {
  @Expose()
  @IsNotEmpty()
  @IsString()
  username: string;

  @Expose()
  @IsNotEmpty()
  @IsString()
  password: string;

  @Expose()
  @IsNotEmpty()
  @IsString()
  displayName: string;

  @Expose()
  @IsNotEmpty()
  @IsEmail()
  @IsString()
  email: string;

  @Expose()
  @IsNotEmpty()
  @IsEnum(USER_ROLES)
  role: USER_ROLES;
}
