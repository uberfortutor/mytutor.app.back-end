import { Expose } from 'class-transformer';
import { IsNotEmpty, IsString, IsEmail } from 'class-validator';

export class ResetPasswordThroughEmailDTO {
  @Expose()
  @IsNotEmpty()
  @IsString()
  @IsEmail()
  email: string;
}
