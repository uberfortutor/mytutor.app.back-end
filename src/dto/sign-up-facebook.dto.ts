import { SignUpUserDTO } from './sign-up-user.dto';
import { IsNotEmpty, IsString, IsEmail, IsOptional, IsEnum } from 'class-validator';
import { USER_ROLES } from '../commons/defines/enums/user-role.enum';
import { Expose } from 'class-transformer';

export class SignUpFacebookDTO {
  @Expose()
  @IsNotEmpty()
  @IsString()
  facebookId: string;

  @Expose()
  @IsNotEmpty()
  @IsString()
  displayName: string;

  @Expose()
  @IsNotEmpty()
  @IsString()
  username: string;

  @Expose()
  @IsNotEmpty()
  @IsString()
  password: string;

  @Expose()
  @IsOptional()
  @IsEmail()
  @IsString()
  email?: string;

  @Expose()
  @IsNotEmpty()
  @IsEnum(USER_ROLES)
  role: USER_ROLES;
}
