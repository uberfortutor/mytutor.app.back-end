import { IsNotEmpty, IsString } from 'class-validator';
import { Expose } from 'class-transformer';

export class CreateSkillDTO {
  @Expose()
  @IsNotEmpty()
  @IsString()
  title?: string;
}
