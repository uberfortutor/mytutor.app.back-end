import { IsString, IsNotEmpty } from 'class-validator';
import { Expose } from 'class-transformer';

export class SignInDTO {
  @IsNotEmpty()
  @IsString()
  @Expose()
  username?: string;

  @IsNotEmpty()
  @IsString()
  @Expose()
  password?: string;
}
