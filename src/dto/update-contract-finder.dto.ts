import { Expose } from 'class-transformer';
import { IsOptional, IsString, IsNumber } from 'class-validator';

export class UpdateContractFinderDTO {
  @Expose()
  @IsOptional()
  @IsString()
  teach?: string;

  @Expose()
  @IsOptional()
  @IsNumber()
  tuition?: number;
}
