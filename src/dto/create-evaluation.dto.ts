import { Expose } from 'class-transformer';
import { IsNotEmpty, IsString, IsNumber } from 'class-validator';

export class CreateEvalulationDTO {
  @Expose()
  @IsNotEmpty()
  @IsString()
  content?: string;

  @Expose()
  @IsNotEmpty()
  @IsNumber()
  rate?: number;
}
