import {
  IsOptional,
  IsNotEmpty,
  IsString,
  IsEmail,
  IsEnum,
  IsDateString,
  IsNotIn,
} from 'class-validator';
import { GENDERS, PROVINCES } from '../commons/defines/enums';
import { Expose } from 'class-transformer';
import { IsDateFormat } from '../commons/custom-validator-decorators/is-date-format.validator-decorator';

export class UpdateProfileDTO {
  @Expose()
  @IsOptional()
  @IsString()
  displayName?: string;

  @Expose()
  @IsNotEmpty()
  @IsOptional()
  @IsEmail()
  @IsString()
  email?: string;

  @Expose()
  @IsOptional()
  @IsString()
  phone?: string;

  @Expose()
  @IsNotEmpty()
  @IsOptional()
  @IsEnum(GENDERS)
  gender?: GENDERS;

  @Expose()
  @IsOptional()
  @IsDateFormat('YYYY-MM-DD', { message: 'birthday must be format YYYY-MM-DD' })
  @IsString()
  birthday?: string;

  @Expose()
  @IsOptional()
  @IsString()
  province?: string;

  @Expose()
  @IsOptional()
  @IsString()
  ward?: string;

  @Expose()
  @IsOptional()
  @IsString()
  detail?: string;
}
