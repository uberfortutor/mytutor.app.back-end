import { Expose } from 'class-transformer';
import { IsString, IsNotEmpty } from 'class-validator';

export class ResetPasswordDTO {
  @Expose()
  @IsNotEmpty()
  @IsString()
  token: string;

  @Expose()
  @IsNotEmpty()
  @IsString()
  newPassword: string;
}
