import { IsNotEmpty, IsString, IsEmail } from 'class-validator';
import { Expose } from 'class-transformer';

export class CheckExistsEmailDTO {
  @Expose()
  @IsNotEmpty()
  @IsEmail()
  @IsString()
  email: string;
}
