import { registerDecorator, ValidationOptions, ValidationArguments } from 'class-validator';

export function MinNumberString(property: number, validationOptions?: ValidationOptions) {
  return function(object: Record<string, any>, propertyName: string) {
    registerDecorator({
      name: 'minNumberString',
      target: object.constructor,
      propertyName: propertyName,
      constraints: [property],
      options: validationOptions,
      validator: {
        validate(value: number, args: ValidationArguments) {
          const relatedValue = args.constraints[0];
          return value >= relatedValue;
        },
      },
    });
  };
}
