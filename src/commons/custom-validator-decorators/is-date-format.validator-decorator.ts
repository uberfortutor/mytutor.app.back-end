import { registerDecorator, ValidationOptions, ValidationArguments } from 'class-validator';
import moment = require('moment');
const DATE_FORMAT = {
  'YYYY-MM-DD': /^[0-9]{1,4}-((0[1-9]{1})|(1[0-2]{1}))-[0-9]{2}$/,
  'YYYY/MM/DD': /^[0-9]{1,4}\/((0[1-9]{1})|(1[0-2]{1}))\/[0-9]{2}$/,
  // 'DD/MM/YYYY': /^[0-9]{1,4}\/((0[1-9]{1})|(1[0-2]{1}))\/[0-9]{2}$/,
};

export function IsDateFormat(property: string, validationOptions?: ValidationOptions) {
  return function(object: Record<string, any>, propertyName: string) {
    registerDecorator({
      name: 'isDateFormat',
      target: object.constructor,
      propertyName: propertyName,
      constraints: [property],
      options: validationOptions,
      validator: {
        validate(value: string, args: ValidationArguments) {
          const relatedValue = args.constraints[0];
          return (
            value.search(DATE_FORMAT[relatedValue]) === 0 &&
            moment(value, relatedValue).format(relatedValue) !== 'Invalid date'
          );
        },
      },
    });
  };
}
