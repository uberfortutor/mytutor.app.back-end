export const commonConfigs = {
  AUTHORIZATION_ACCOUNT_HOST: 'https://mytutor-backend.herokuapp.com',
  PROD_HOST: 'https://mytutor-backend.herokuapp.com',
  PAGE_SIZE: 12,
};
