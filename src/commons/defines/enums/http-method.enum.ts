export enum HTTP_METHOD_DATA_FIELD {
  POST = 'body',
  GET = 'query',
}
