export enum PROVINCES {
  DEFAULT = '',
  ALL = 'all',
  BD = 'bd',
  HCM = 'hcm',
}
