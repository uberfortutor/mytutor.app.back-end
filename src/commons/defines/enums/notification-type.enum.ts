export enum NOTIFICATION_TYPE {
  MESSAGE = 'message',
  CONTRACT = 'contract',
}
