export enum USER_ROLES {
  FINDER = 'finder',
  TUTOR = 'tutor',
}
