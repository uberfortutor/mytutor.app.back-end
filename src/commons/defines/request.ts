import { Request } from 'express';
import { MTTError } from './error';
import { User } from '../../database/entities/user.entity';
export interface MTTRequest extends Request {
  mttError?: MTTError;
  mttRes?: any;
  user?: User;
  info?: any;
}
