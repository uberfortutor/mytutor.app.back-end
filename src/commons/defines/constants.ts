import { Contract } from '../../database/entities/contract.entity';
import { User } from '../../database/entities/user.entity';

export const USER_FIELDS_NEED_TO_REMOVE: string[] = [
  'username',
  'password',
  'isNew',
  'authorizated',
  'lock',
  'authorizationToken',
  'delete',
  'avatarId',
  'googleId',
  'facebookId',
];

export const USER_FIELDS_NEED_TO_GET: (keyof User)[] = [
  'id',
  'displayName',
  'email',
  'phone',
  'gender',
  'birthday',
  'role',
  'avatar',
];

export const CONTRACT_FIELDS_NEED_TO_GET: (keyof Contract)[] = [
  'id',
  'teach',
  'tuitionOfFinder',
  'confirmedTuition',
  'status',
  'numberOfSession',
  'numberOfHourPerSession',
  'createdDate',
  'updatedDate',
];
