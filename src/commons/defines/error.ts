import { HTTP_CODE } from './enums';

export interface MTTError {
  code?: HTTP_CODE;
  message?: string;
}
