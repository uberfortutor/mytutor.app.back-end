import { join } from 'path';
import * as swaggerUI from 'swagger-ui-express';
import * as swaggerDocument from './swagger.json';

const options = {
  swaggerOptions: {
    // url: join(__dirname, './swagger.json'),
    url: './swagger.json',
  },
};

export const configSwagger = app => {
  // console.log(swaggerDocument);
  app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(swaggerDocument));
};
