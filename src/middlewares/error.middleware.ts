import { MTTRequest } from '../commons/defines/request';
import { Response } from 'express';

export const handleError = (req: MTTRequest, res: Response, next: any) => {
  if (req.mttError) {
    res.status(req.mttError.code).send(req.mttError.message);
  }
};
