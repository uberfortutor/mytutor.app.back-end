import { MTTRequest } from '../commons/defines/request';
import passport = require('passport');
import { HTTP_CODE } from '../commons/defines/enums';

export const signIn = (req, res, next) => {
  console.log('auth user');
  passport.authenticate('local', { session: true }, (error, user, info) => {
    if (error) {
      return res.status(HTTP_CODE.INTERNAL_SERVER_ERROR).send('errors');
    }

    req.info = info;
    req.user = user || null;
    next();
  })(req, res, next);
};
