import * as multer from 'multer';
import { get } from 'lodash';

const createMulterImageConfig = (destPath, fieldPathFromReq = []) => ({
  storage: multer.diskStorage({
    destination: function(req, file, next) {
      next(null, destPath);
    },

    //Then give the file a unique name
    filename: function(req, file, next) {
      const ext = file.mimetype.split('/')[1];
      const filename = `${get(req, fieldPathFromReq)}.${ext}`;
      req.avatar = {
        image: destPath + `/${filename}`,
        ext,
      };
      next(null, filename);
    },
  }),

  //A means of ensuring only images are uploaded.
  fileFilter: function(req, file, next) {
    if (!file) {
      next();
    }
    const image = file.mimetype.startsWith('image/');
    if (image) {
      // console.log('photo uploaded');
      next(null, true);
    } else {
      console.log('file not supported');

      //TODO:  A better message response to user on failure.
      return next();
    }
  },
});

export const uploadImageFile = (field, fieldPathFromReq, destPath) => {
  return multer(createMulterImageConfig(destPath, fieldPathFromReq)).single(field);
};
