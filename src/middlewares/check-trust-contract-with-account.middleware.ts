import { get } from 'lodash';
import { contractService } from '../services';
import { USER_ROLES } from '../commons/defines/enums';

export const checkTrustContractWithAccount = (req: any, res: any, next: any) => {
  const userId = get(req, ['user', 'id']);
  const role = get(req, ['user', 'role']);
  const contractId = get(req, ['params', 'id']);

  contractService
    .isExists(
      {
        id: contractId,
        delete: false,
        ...(role === USER_ROLES.FINDER ? { finder: { id: userId } } : { tutor: { id: userId } }),
      },
      'id',
    )
    .then(exists => {
      if (exists) {
        return next();
      }

      res.status(403).send('Bạn không có quyền đối với hợp đồng này');
    });
};
