import { MTTRequest } from '../commons/defines/request';
import { Response } from 'express';
import { HTTP_CODE } from '../commons/defines/enums';

export const handleNotFound = (req: MTTRequest, res: Response, next: any) => {
  if (!req.mttError) {
    req.mttError = {
      code: HTTP_CODE.NOT_FOUND,
      message: 'Not found',
    };
  }

  next();
};
