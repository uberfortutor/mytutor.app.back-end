import { get } from 'lodash';
import { HTTP_CODE } from '../commons/defines/enums';

export const checkAuthorizatedAccount = (req: any, res: any, next: any) => {
  if (!get(req, ['user', 'authorizated'], false)) {
    return res.status(HTTP_CODE.FORBIDDEN).json({ unauthorizatedEmail: true });
  }

  next();
};
