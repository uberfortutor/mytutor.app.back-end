import { get, keysIn } from 'lodash';
import { BaseService } from '../services/base.service';

const initCondition = (conditionPattern: any, data: any) => {
  const condition = {};

  keysIn(conditionPattern).forEach(field => {
    if (typeof field === 'string') {
      condition[field] = get(data, conditionPattern[field]);
    } else {
      condition[field] = initCondition(conditionPattern[field], data);
    }
  });

  return condition;
};

export const checkExistsWithConditionsFromReq = (
  service: BaseService<any>,
  conditions: any,
  dataPath: string,
  fieldToCheckExists: string,
  exceptionResponse: {
    code: number;
    message: string;
  },
) => (req: any, res: any, next: any) => {
  const condition = initCondition(conditions, get(req, dataPath));
  service.isExists(condition, fieldToCheckExists).then(exists => {
    if (exists) {
      return next();
    }

    res.status(exceptionResponse.code).send(exceptionResponse.message);
  });
};
