import { get } from 'lodash';
import { HTTP_METHOD_DATA_FIELD, HTTP_CODE } from '../commons/defines/enums';
import { userService } from '../services';

export const checkNotExistsEmail = (req: any, res: any, next: any) => {
  const email = get(req, [HTTP_METHOD_DATA_FIELD[req.method], 'email']);

  userService.isExists({ email, delete: false }, 'id').then(exists => {
    if (!exists) {
      return res
        .status(HTTP_CODE.OK)
        .json({ status: false, message: 'Email này chưa được đăng ký.' });
    }

    next();
  });
};
