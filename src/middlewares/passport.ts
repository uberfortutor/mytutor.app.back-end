import * as passport from 'passport';
import { get } from 'lodash';
import { Strategy as JWTStrategy, ExtractJwt } from 'passport-jwt';
import { Strategy as LocalStrategy } from 'passport-local';
import { Strategy as GoogleStrategy } from 'passport-google-oauth20';
import { Strategy as FacebookStrategy } from 'passport-facebook';
import * as bcrypt from 'bcrypt';
import { JWTConfig } from '../commons/configs';
import { userService } from '../services';
import { googleConfigs } from '../commons/configs/google.config';
import { facebookConfigs } from '../commons/configs/facebook.config';
import { USER_FIELDS_NEED_TO_GET } from '../commons/defines/constants';

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
export const configPassportStrategies = () => {
  passport.serializeUser(function(user, done) {
    done(null, user);
  });

  passport.deserializeUser(function(user, done) {
    done(null, user);
  });

  passport.use(
    new LocalStrategy(
      {
        usernameField: 'username',
        passwordField: 'password',
      },
      (username, password, cb) => {
        userService
          .findOne({
            where: {
              username,
              delete: false,
            },
            select: ['id', 'username', 'password', 'role', 'isNew', 'authorizated', 'lock'],
          })
          .then(user => {
            if (!user) {
              return cb(null, false, {
                message: 'Incorrect username or password',
              });
            }

            if (!bcrypt.compareSync(password, user.password)) {
              return cb(null, false, {
                message: 'Incorrect username or password',
              });
            }

            return cb(null, user, { message: 'Sign in successfully' });
          })
          .catch(err => {
            console.log('err', err);
            cb(err, false, { message: 'error' });
          });
      },
    ),
  );

  passport.use(
    new JWTStrategy(
      {
        jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
        secretOrKey: JWTConfig.SECRET_KEY,
      },
      function(jwtPayload, cb) {
        userService
          .findOne({
            where: { id: jwtPayload.id, delete: false },
            select: [
              ...USER_FIELDS_NEED_TO_GET,
              'username',
              'avatarId',
              'isNew',
              'authorizated',
              'lock',
            ],
            relations: ['address'],
          })
          .then(user => {
            if (user) {
              return cb(null, user);
            }

            return cb(null, null);
          })
          .catch(err => cb(err, null));
      },
    ),
  );

  passport.use(
    new GoogleStrategy(
      {
        clientID: googleConfigs.CLIENT_ID,
        clientSecret: googleConfigs.CLIENT_SECRET,
        callbackURL: googleConfigs.CALLBACK_URL,
        profileFields: ['email', 'birthday', 'gender'],
      },
      function(accessToken, refreshToken, profile, cb) {
        console.log('profile', profile);
        userService
          .findOne({
            where: { googleId: profile.id, delete: false },
            select: ['id', 'username', 'role', 'isNew', 'authorizated', 'lock'],
          })
          .then(user => {
            if (user) {
              return cb(null, { exists: true, data: user });
            }

            return cb(null, {
              exists: false,
              data: {
                googleId: profile.id,
                displayName: profile.displayName,
                email: get(profile, ['emails', 0, 'value']),
              },
            });
          })
          .catch(err => cb(err, null));
      },
    ),
  );

  passport.use(
    new FacebookStrategy(
      {
        clientID: facebookConfigs.CLIENT_ID,
        clientSecret: facebookConfigs.CLIENT_SECRET,
        callbackURL: facebookConfigs.CALLBACK_URL,
        profileFields: ['id', 'displayName', 'photos', 'emails'],
      },
      async function(accessToken, refreshToken, profile, cb) {
        console.log('profile', profile);
        userService
          .findOne({
            where: { facebookId: profile.id, delete: false },
            select: ['id', 'username', 'role', 'isNew', 'authorizated', 'lock'],
          })
          .then(user => {
            if (user) {
              return cb(null, { exists: true, data: user });
            }

            cb(null, {
              exists: false,
              data: {
                facebookId: profile.id,
                displayName: profile.displayName,
                email: get(profile, ['emails', 0, 'value']),
              },
            });
          })
          .catch(err => cb(err, null));
      },
    ),
  );
};
