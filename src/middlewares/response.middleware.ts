import { MTTRequest } from '../commons/defines/request';
import { Response } from 'express';
import { HTTP_CODE } from '../commons/defines/enums';

export const handleResponse = (req: MTTRequest, res: Response, next: any) => {
  if (!req.mttError && req.mttRes) {
    return res.status(HTTP_CODE.OK).json(req.mttRes);
  }

  next();
};
