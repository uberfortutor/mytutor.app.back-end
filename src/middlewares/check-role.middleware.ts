import { get } from 'lodash';
import { HTTP_CODE, USER_ROLES } from '../commons/defines/enums';

export const checkRole = (...roles: USER_ROLES[]) => (req: any, res: any, next: any) => {
  if (roles.some(role => get(req, ['user', 'role']) === role)) {
    return next();
  }

  res.status(HTTP_CODE.FORBIDDEN).json({ roleNotAllowed: true });
};
