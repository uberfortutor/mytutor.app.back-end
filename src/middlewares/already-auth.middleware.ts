import passport = require('passport');
import { MTTRequest } from '../commons/defines/request';
import { HTTP_CODE } from '../commons/defines/enums';
export const alreadyAuth = (req: any, res: any, next: any) => {
  passport.authenticate('jwt', { session: false }, (err, user) => {
    if (err) {
      return res.status(HTTP_CODE.INTERNAL_SERVER_ERROR).send('error');
    }

    if (user) {
      req.user = user;
      return next();
    }

    res.status(HTTP_CODE.UNAUTHORIZATED).send('Unauthorizated');
  })(req, res, next);
};
