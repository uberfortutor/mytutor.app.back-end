import { validate } from 'class-validator';
import { plainToClass } from 'class-transformer';
import { HTTP_CODE, HTTP_METHOD_DATA_FIELD } from '../commons/defines/enums';
import { removeUndefinedField } from '../utils/object.util';

export const validateDTO = (dtoDefinition: any) => async (req: any, res: any, next: any) => {
  const dataToValidate = plainToClass(dtoDefinition, req[HTTP_METHOD_DATA_FIELD[req.method]], {
    excludeExtraneousValues: true,
  });

  console.log('validate DTO: ', dtoDefinition, dataToValidate);

  validate(dataToValidate).then(errors => {
    if (errors.length > 0) {
      console.log('validate DTO errors: ', errors);
      return res.status(HTTP_CODE.BAD_REQUEST).send('Bad request');
    }

    req[HTTP_METHOD_DATA_FIELD[req.method]] = removeUndefinedField(dataToValidate);
    next();
  });
};
