import { MTTRequest } from '../commons/defines/request';
import { userService } from '../services';
import { HTTP_CODE } from '../commons/defines/enums';

export const checkExistsSocialAccount = (isFb: boolean) => async (
  req: MTTRequest,
  res: any,
  next: any,
) => {
  const socialId = isFb ? req.body.facebookId : req.body.googleId;

  if (
    socialId &&
    (await userService.isExists(
      {
        googleId: isFb ? null : socialId,
        facebookId: isFb ? socialId : null,
        delete: false,
      },
      'id',
    ))
  ) {
    return res.status(HTTP_CODE.OK).json({
      status: false,
      message: `Đã có tài khoản tồn tại với tài khoản ${isFb ? 'facebook' : 'google'} này`,
    });
  }

  next();
};
