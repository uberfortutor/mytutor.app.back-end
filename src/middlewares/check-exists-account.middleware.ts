import { MTTRequest } from '../commons/defines/request';
import { userService } from '../services';
import { HTTP_CODE } from '../commons/defines/enums';

export const checkExistsAccount = async (req: MTTRequest, res: any, next: any) => {
  const username = req.body.username;

  if (
    username &&
    (await userService.findOne({
      where: { username, delete: false },
      select: ['id'],
    }))
  ) {
    return res.status(HTTP_CODE.OK).json({
      status: false,
      message: 'Username đã tồn tại. Hãy chọn username khác',
    });
  }

  next();
};
