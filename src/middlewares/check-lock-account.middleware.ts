import { get } from 'lodash';
import { HTTP_CODE } from '../commons/defines/enums';

export const checkLockAccount = (req: any, res: any, next: any) => {
  if (get(req, ['user', 'lock'])) {
    return res.status(HTTP_CODE.FORBIDDEN).end('Locked');
  }

  next();
};
