import { set, get } from 'lodash';
import * as moment from 'moment';

export const convertDateFormat = (
  dataObjectPathFromReq: string,
  datafieldsPathToConvert: string[],
) => (req: any, res: any, next: any) => {
  const data = get(req, dataObjectPathFromReq);

  datafieldsPathToConvert.map(field => {
    get(data, field) &&
      set(data, field, moment(get(data, field), 'MM/DD/YYYY').format('YYYY-MM-DD'));
  });

  set(req, dataObjectPathFromReq, data);

  next();
};
