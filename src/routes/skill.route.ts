import { Router } from 'express';
import { get, isEmpty } from 'lodash';
import { skillService, userService } from '../services';
import {
  validateDTO,
  alreadyAuth,
  checkLockAccount,
  checkAuthorizatedAccount,
  checkRole,
} from '../middlewares';
import { CreateSkillDTO } from '../dto';
import { USER_ROLES } from '../commons/defines/enums';

const router = Router();

router.get('/', (req: any, res: any) => {
  skillService
    .find({ where: { delete: false }, select: ['id', 'title'] })
    .then(skills => {
      res.status(200).json({ skills });
    })
    .catch(err => {
      console.log('get skills error: ', err);
      res.status(500).send('Lỗi');
    });
});

router.post(
  '/',
  alreadyAuth,
  checkLockAccount,
  checkAuthorizatedAccount,
  checkRole(USER_ROLES.TUTOR),
  validateDTO(CreateSkillDTO),
  (req: any, res: any) => {
    userService
      .addNewSkill(get(req, ['user', 'id']), get(req, ['body', 'title']))
      .then(skill => res.status(200).json({ status: !isEmpty(skill), skill }))
      .catch(err => {
        console.log('create skill error: ', err);
        res.status(500).send('Lỗi');
      });
  },
);

export const skillRouter = router;
