import { Router } from 'express';
import { get } from 'lodash';
import { MTTRequest } from '../commons/defines/request';
import { userService, tutorService } from '../services';
import { commonConfigs } from '../commons/configs';
import {
  validateDTO,
  checkExistsWithConditionsFromReq,
  alreadyAuth,
  checkLockAccount,
  checkAuthorizatedAccount,
  checkRole,
} from '../middlewares';
import { GetTutorsDTO } from '../dto/get-tutors.dto';
import { tutorEvaluationService } from '../services/tutor-evaluation.service';
import { USER_ROLES } from '../commons/defines/enums';
import { CreateEvalulationDTO } from '../dto/create-evaluation.dto';
const router = Router();

router.get('/', validateDTO(GetTutorsDTO), async (req: MTTRequest, res: any, next: any) => {
  const { province, ward } = req.query;
  const increaseTuition = get(req, ['query', 'increaseTuition']) === 'false' ? false : true;
  const page = parseInt(get(req, ['query', 'page'], 1), 10);
  const pageSize = parseInt(get(req, ['query', 'pageSize'], commonConfigs.PAGE_SIZE));
  const skills = (req.query.skills || '')
    .split(',')
    .filter(skillId => skillId !== '')
    .map(skillId => parseInt(skillId, 10));
  const minTuition = parseFloat(get(req, ['query', 'minTuition'], 0));
  const maxTuition = parseFloat(get(req, ['query', 'maxTuition'], 0));

  userService
    .getTutors(province, ward, skills, minTuition, maxTuition, page, pageSize, increaseTuition)
    .then(users => {
      req.mttRes = { tutors: users };
      next();
    })
    .catch(err => {
      console.log('get tutors error: ', err);
      req.mttError = { code: 500, message: 'Lỗi' };
      next();
    });
});

router.get('/high-rate', (req: MTTRequest, res: any, next: any) => {
  userService
    .getHighRateTutors()
    .then(tutors => {
      res.status(200).json({ tutors });
    })
    .catch(err => {
      console.log('get high rate tutors error: ', err);
      res.status(500).send('Không thể lấy dữ liệu');
    });
});

router.get('/:userId', (req: MTTRequest, res: any, next: any) => {
  const id = get(req, ['params', 'userId'], null);

  userService
    .getTutor(id)
    .then(user => {
      req.mttRes = { tutor: user };
      next();
    })
    .catch(err => {
      console.log('get tutor detail error: ', err);
      req.mttError = {
        code: 500,
        message: 'Lỗi',
      };
      next();
    });
});

router.get('/:userId/evaluations', (req: any, res: any, next: any) => {
  const { userId } = get(req, 'params', {});
  const { page = 1, pageSize = commonConfigs.PAGE_SIZE } = get(req, 'query', {});

  tutorEvaluationService
    .getEvaluations(userId, page, pageSize)
    .then(evaluations => res.status(200).json({ evaluations }))
    .catch(err => res.status(500).send('Không thể lấy dữ liệu'));
});

router.post(
  '/:userId/evaluations',
  alreadyAuth,
  checkLockAccount,
  checkAuthorizatedAccount,
  checkRole(USER_ROLES.FINDER),
  validateDTO(CreateEvalulationDTO),
  (req: any, res: any, next: any) => {
    const finderId: number = get(req, ['user', 'id']);
    const tutorId: number = get(req, ['params', 'userId']);
    const { content, rate } = get(req, 'body', {});

    tutorEvaluationService
      .createEvaluation(finderId, tutorId, content, rate)
      .then(async ({ status, evaluation }) => {
        const tutor = await userService.findOne({
          where: {
            id: tutorId,
          },
          select: ['id'],
          relations: ['tutor'],
        });

        let tutorRate = tutor.tutor.rate;
        let tutorNumberOfEvaluation = tutor.tutor.numberOfEvaluation;

        tutorRate = tutorRate * tutorNumberOfEvaluation + rate;
        tutorNumberOfEvaluation++;
        tutorRate /= tutorNumberOfEvaluation;

        await tutorService.save([
          { id: tutor.tutor.id, rate: tutorRate, numberOfEvaluation: tutorNumberOfEvaluation },
        ]);

        res.status(200).json({
          status,
          evaluation,
          newRate: tutorRate,
          newNumberOfEvaluation: tutorNumberOfEvaluation,
        });
      })
      .catch(err => res.status(500).send('Không thể thêm đánh giá'));
  },
);

export const tutorRouter = router;
