import { Router } from 'express';
const router = Router();
import * as jwt from 'jsonwebtoken';
import * as passport from 'passport';
import { get, pick, isEmpty, omit } from 'lodash';
import { MTTRequest } from '../commons/defines/request';
import {
  checkExistsAccount,
  validateDTO,
  signIn,
  alreadyAuth,
  checkAuthorizatedAccount,
  checkLockAccount,
  checkExistsSocialAccount,
  convertDateFormat,
  checkNotExistsEmail,
} from '../middlewares';
import { userService } from '../services';
import { JWTConfig, commonConfigs } from '../commons/configs';
import {
  SignUpUserDTO,
  SignInDTO,
  SignUpFacebookDTO,
  SignUpGoogleDTO,
  CheckExistsUsernameDTO,
  UpdateProfileDTO,
  UpdateTutorInfoDTO,
  ChangePasswordDTO,
  CheckExistsEmailDTO,
  ResetPasswordDTO,
  ResetPasswordThroughEmailDTO,
} from '../dto';
import { checkRole } from '../middlewares/check-role.middleware';
import { USER_ROLES, HTTP_CODE } from '../commons/defines/enums';
import { checkExistsEmail } from '../middlewares/check-exists-email.middleware';
import { Not } from 'typeorm';
import { uploadImageFile } from '../middlewares/multer.middleware';
import { join } from 'path';
import { googleDriveService } from '../services/gg-drive.service';
import { UploadFileResponse } from 'google-drive-client/src/defines/responses';
import { hashPassword } from '../utils/hash-password.util';

router.post(
  '/sign-in',
  validateDTO(SignInDTO),
  signIn,
  (req, res, next) => {
    if (!req.user) {
      return res.status(HTTP_CODE.UNAUTHORIZATED).end('Unauthorizated');
    }

    next();
  },
  checkLockAccount,
  checkAuthorizatedAccount,
  (req: MTTRequest, res, next) => {
    req.mttRes = {
      token: jwt.sign(
        Object.assign({}, omit(get(req, 'user', null)), ['password', 'role']),
        JWTConfig.SECRET_KEY,
      ),
      metadata: {
        role: get(req.user, ['role'], null),
        isNew: get(req, ['user', 'isNew'], null),
      },
      message: 'Đăng nhập thành công',
    };
    next();
  },
);

router.post(
  '/sign-up',
  validateDTO(SignUpUserDTO),
  checkExistsEmail,
  checkExistsAccount,
  (req: MTTRequest, res: any, next: any) => {
    const data: SignUpUserDTO = req.body;

    userService
      .signUp(data)
      .then(userId => {
        if (userId) {
          userService
            .sendAuthorizationEmail(userId)
            .then(token => console.log('send email token', token))
            .catch(err => {
              console.log('send mail error', err);
            });
        }
        req.mttRes = {
          status: userId ? true : false,
          message: userId ? 'Đăng ký thành công' : 'Đăng ký thất bại',
        };

        return next();
      })
      .catch(err => {
        console.log('error', err);
        req.mttError = {
          code: 500,
          message: 'error',
        };

        return next();
      });
  },
);

// not document
router.post(
  '/send-authorization-email',
  validateDTO(SignInDTO),
  signIn,
  (req: MTTRequest, res: any, next: any) => {
    if (!req.user) {
      req.mttRes = { status: false, message: 'Tài khoản này chưa được đăng ký' };
      return next();
    }

    if (req.user.authorizated) {
      req.mttRes = { status: false, message: 'Tài khoản của bạn đã được xác thực' };
      return next();
    }

    userService
      .sendAuthorizationEmail(req.user.id)
      .then(token => {
        console.log('token', token);
        req.mttRes = { status: true };
        next();
      })
      .catch(err => {
        console.log('err', err);
        req.mttError = { code: 500, message: 'error' };
        next();
      });
  },
);

router.get('/authorizate-account/:token', (req: MTTRequest, res, next) => {
  const token = req.params.token;

  if (!token) {
    const html = `<div style="width: 100%;">
      <img
        style="width: 50px; height: 50px; margin: 20px auto;" 
        src="https://mytutor-backend.herokuapp.com/logo.png" 
        alt="logo" />
        <div>
          <div style="text-align: center; font-size: 20px; font-weight: bold; color: #1aa3ff;">
            Đường dẫn kích hoạt không hợp lệ, vui lòng gửi yêu cầu gửi lại email kích hoạt tại đường dẫn:<br />
            <a href="">Gửi lại email kích hoạt tài khoản</a>
          </div>
        </div>
   </div>`;
    return res.send(html);
  }

  userService
    .isValidAuthorizationToken(token)
    .then(status => {
      const html = `<div style="width: 100%;">
        <img
          style="display: block; width: 50px; height: 50px; margin: 20px auto;" 
          src="https://mytutor-backend.herokuapp.com/logo.png" 
          alt="logo" />
          <div>
            <div style="text-align: center; font-size: 20px; font-weight: bold; color: #1aa3ff;">
              Kích hoạt tài khoản ${status ? '' : 'không'} thành công
            </div>
            <div style="text-align: center">
              ${
                status
                  ? 'Tài khoản của bạn đã được kích hoạt. Bây giờ bạn có thể đăng nhập vào MyTutor để tiến hành đặt gia sử hoặc làm gia sư'
                  : 'Tài khoản của bạn chưa được kích hoạt. Bạn có thể yêu cầu gửi email kích hoạt lần nữa tại link: <a href="">Gửi lại email kích hoạt</a>'
              }
            </div>
          </div>
      </div>`;

      return res.send(html);
    })
    .catch(err => {
      req.mttError = { code: 500, message: 'error' };
      next();
    });
});

router.get(
  '/facebook',
  passport.authenticate('facebook', {
    scope: ['email'],
  }),
);

router.get(
  '/facebook/callback',
  passport.authenticate('facebook'),
  (req: any, res: any, next: any) => {
    let resData = null;
    const lock = get(req, ['user', 'data', 'lock']);

    // console.log('facebook auth res: ', req.user);
    if (get(req, ['user', 'exists'])) {
      resData = {
        exists: true,
        data: {
          token: lock
            ? null
            : jwt.sign(Object.assign({}, get(req, ['user', 'data'])), JWTConfig.SECRET_KEY),
          metadata: pick(get(req, ['user', 'data'], {}), ['role', 'isNew', 'authorizated', 'lock']),
          message: lock ? 'Tài khoản của bạn đã bị khóa' : null,
        },
      };
    } else {
      resData = { exists: false, data: get(req, ['user', 'data'], {}) };
    }

    res
      .status(200)
      .send(
        `
      <html>
        <body>
          <script>
            window.opener.postMessage(${JSON.stringify({
              type: 'LOGIN_VIA_SOCIAL',
              messageData: resData,
            })}, '*');
            window.close();
          </script>
        </body>
      </html>
      `,
      )
      .end();
  },
);

router.post(
  '/facebook/sign-up',
  validateDTO(SignUpFacebookDTO),
  checkExistsSocialAccount(true),
  checkExistsEmail,
  checkExistsAccount,
  (req: MTTRequest, res: any, next: any) => {
    userService
      .signUp(req.body, true)
      .then(async userId => {
        if (userId) {
          const user = await userService.findOne({
            where: { id: userId },
            select: ['id', 'username', 'role', 'isNew', 'authorizated', 'lock'],
          });

          req.mttRes = {
            status: true,
            message: 'Đăng ký thành công',
            data: {
              token: jwt.sign(Object.assign({}, user), JWTConfig.SECRET_KEY),
              metadata: pick(user, ['role', 'isNew', 'authorizated', 'lock']),
            },
          };
          return next();
        }

        req.mttRes = {
          status: false,
          message: 'Bạn không thể tạo tài khoản với dữ liệu bạn cung cấp: ' + req.body,
        };
        next();
      })
      .catch(err => {
        req.mttError = { code: 500, message: 'Lỗi' };
        next();
      });
  },
);

router.get(
  '/google',
  passport.authenticate('google', {
    scope: ['profile', 'email', 'https://www.googleapis.com/auth/user.birthday.read'],
  }),
);

router.get('/google/callback', passport.authenticate('google'), (req: any, res: any, next: any) => {
  let resData = null;
  const lock = get(req, ['user', 'data', 'lock']);
  console.log('user', req.user);
  if (get(req, ['user', 'exists'])) {
    resData = {
      exists: true,
      data: {
        token: lock
          ? null
          : jwt.sign(Object.assign({}, get(req, ['user', 'data'], {})), JWTConfig.SECRET_KEY),
        metadata: pick(get(req, ['user', 'data'], {}), ['role', 'isNew', 'authorizated', 'lock']),
        message: lock ? 'Tài khoản của bạn đã bị khóa' : null,
      },
    };
  } else {
    resData = { exists: false, data: get(req, ['user', 'data'], {}) };
  }

  res
    .status(200)
    .send(
      `
    <html>
      <body>
        <script>
          window.opener.postMessage(${JSON.stringify({
            type: 'LOGIN_VIA_SOCIAL',
            messageData: resData,
          })}, '*');
          window.close();
        </script>
      </body>
    </html>
    `,
    )
    .end();
});

router.post(
  '/google/sign-up',
  validateDTO(SignUpGoogleDTO),
  checkExistsSocialAccount(false),
  checkExistsEmail,
  checkExistsAccount,
  (req: MTTRequest, res: any, next: any) => {
    userService
      .signUp(req.body, true)
      .then(async userId => {
        if (userId) {
          const user = await userService.findOne({
            where: { id: userId },
            select: ['id', 'username', 'role', 'isNew', 'authorizated', 'lock'],
          });

          req.mttRes = {
            status: true,
            message: 'Đăng ký thành công',
            data: {
              token: jwt.sign(Object.assign({}, user), JWTConfig.SECRET_KEY),
              metadata: pick(user, ['role', 'isNew', 'authorizated', 'lock']),
            },
          };
          return next();
        }

        req.mttRes = {
          status: false,
          message: 'Bạn không thể tạo tài khoản với dữ liệu bạn cung cấp: ' + req.body,
        };
        next();
      })
      .catch(err => {
        console.log('err', err);
        req.mttError = { code: 500, message: 'error' };
        next();
      });
  },
);

router.post(
  '/check-exists-username',
  validateDTO(CheckExistsUsernameDTO),
  (req: MTTRequest, res: any, next: any) => {
    userService
      .isExists({ username: get(req, ['body', 'username']), delete: false }, 'id')
      .then(exists => {
        req.mttRes = { exists };
        next();
      })
      .catch(err => {
        req.mttError = { code: 500, message: 'error' };
        next();
      });
  },
);

router.post(
  '/check-exists-email',
  validateDTO(CheckExistsEmailDTO),
  (req: MTTRequest, res: any, next: any) => {
    userService
      .isExists({ email: get(req, ['body', 'email']), delete: false }, 'id')
      .then(exists => {
        req.mttRes = { exists };
        next();
      })
      .catch(err => {
        req.mttError = { code: 500, message: 'Không thể thực hiện' };
        next();
      });
  },
);

router.get(
  '/me/profile',
  alreadyAuth,
  checkLockAccount,
  checkAuthorizatedAccount,
  (req: MTTRequest, res: any, next: any) => {
    userService
      .getProfile(get(req, ['user', 'id']))
      .then(user => {
        req.mttRes = { profile: user };
        next();
      })
      .catch(err => {
        console.log('get profile error: ', err);
        req.mttError = { code: 500, message: 'error' };
        next();
      });
  },
);

router.post(
  '/me/profile',
  alreadyAuth,
  checkLockAccount,
  checkAuthorizatedAccount,
  validateDTO(UpdateProfileDTO),
  async (req: MTTRequest, res: any, next: any) => {
    if (get(req, ['body', 'email'])) {
      if (
        await userService.isExists(
          { id: Not(get(req, ['user', 'id'])), email: get(req, ['body', 'email']), delete: false },
          'id',
        )
      ) {
        return res.status(HTTP_CODE.OK).json({
          status: false,
          profile: null,
          message: 'Email này đã được đăng ký. Hãy chọn email khác.',
        });
      }
    }

    userService
      .updateProfile(req.user.id, req.body)
      .then(user => {
        req.mttRes = {
          status: true,
          profile: user,
          mesage: 'Cập nhật thông tin cá nhân thành công',
        };
        next();
      })
      .catch(err => {
        console.log('update profile error', err);
        req.mttError = { code: HTTP_CODE.INTERNAL_SERVER_ERROR, message: 'Không thể thực hiện.' };
        next();
      });
  },
);

router.get(
  '/me/tutor-info',
  alreadyAuth,
  checkLockAccount,
  checkAuthorizatedAccount,
  checkRole(USER_ROLES.TUTOR),
  (req: MTTRequest, res: any, next: any) => {
    userService
      .getTutorInfo(req.user.id)
      .then(user => {
        req.mttRes = { tutorInfo: user };
        next();
      })
      .catch(err => {
        console.log('get profile error: ', err);
        req.mttError = { code: 500, message: 'error' };
        next();
      });
  },
);

router.post(
  '/me/tutor-info',
  alreadyAuth,
  checkLockAccount,
  checkAuthorizatedAccount,
  checkRole(USER_ROLES.TUTOR),
  validateDTO(UpdateTutorInfoDTO),
  (req: MTTRequest, res: any, next: any) => {
    userService
      .updateTutorInfo(req.user.id, req.body)
      .then(user => {
        req.mttRes = { tutorInfo: user };
        next();
      })
      .catch(err => {
        console.log('get profile error: ', err);
        req.mttError = { code: 500, message: 'error' };
        next();
      });
  },
);

router.post(
  '/me/avatar',
  alreadyAuth,
  checkLockAccount,
  checkAuthorizatedAccount,
  uploadImageFile('avatar', ['user', 'id'], join(__dirname, '../../public/media/images/users')),
  (req: any, res: any, next: any) => {
    googleDriveService
      .uploadFile(get(req, ['avatar', 'image']), `image/${get(req, ['avatar', 'ext'])}`)
      .then(async (ret: UploadFileResponse) => {
        const userInfo = await userService.findOne({
          where: { id: get(req, ['user', 'id'], null) },
          select: ['id', 'avatarId'],
        });

        if (userInfo.avatarId) {
          googleDriveService
            .deleteFile(userInfo.avatarId)
            .catch(err => console.log('delete file error: ', err));
        }

        userService
          .update(
            { id: get(req, ['user', 'id']) },
            { avatar: get(ret, 'downloadUrl', null), avatarId: get(ret, 'fileId') },
            ['id'],
          )
          .then(user => {
            !user && googleDriveService.deleteFile(ret.fileId);
            req.mttRes = {
              avatar: get(ret, 'downloadUrl', null),
            };
            next();
          })
          .catch(err => {
            console.log('update avatar error: ', err);
            req.mttError = { code: 500, message: 'Không thể cập nhật avatar.' };
            next();
          });
      })
      .catch(err => {
        console.log('update file error: ', err);
        req.mttError = { code: 500, message: 'Không thể cập nhật avatar.' };
        next();
      });
  },
);

router.post(
  '/me/change-password',
  alreadyAuth,
  checkLockAccount,
  checkAuthorizatedAccount,
  validateDTO(ChangePasswordDTO),
  (req: any, res: any, next: any) => {
    const userId = get(req, ['user', 'id']);
    const oldPassword = get(req, ['body', 'oldPassword']);
    const newPassword = get(req, ['body', 'newPassword']);

    userService
      .isTrustPassword(userId, oldPassword)
      .then(status => {
        if (!status) {
          return res.status(200).json({ status: false, message: 'Mật khẩu cũ không đúng' });
        }

        userService
          .changePassword(userId, newPassword)
          .then(status => {
            if (!status) {
              return res.status(200).json({ status: false, message: 'Đổi mật khẩu thất bại' });
            }

            res.status(200).json({ status: true, message: 'Đổi mật khẩu thành công' });
          })
          .catch(err => {
            console.log('change password error: ', err);
            res.status(500).send('Không thể thực hiện');
          });
      })
      .catch(err => {
        console.log('change password error: ', err);
        res.status(500).send('Lỗi');
      });
  },
);

router.post(
  '/reset-password-through-email',
  validateDTO(ResetPasswordThroughEmailDTO),
  checkNotExistsEmail,
  (req: any, res: any, next: any) => {
    userService
      .isExists({ email: get(req, ['body', 'email']), delete: false, lock: false }, 'id')
      .then(exists => {
        if (!exists) {
          return res.status(403).send('Locked');
        }
        next();
      })
      .catch(err => {
        console.log('check exists unlock account error: ', err);
        res.status(500).send('Không thể thực hiện');
      });
  },
  async (req: any, res: any, next: any) => {
    userService
      .sendResetPasswordEmail(get(req, ['body', 'email']))
      .then(token => {
        res.status(200).json({
          status: token ? true : false,
          message: token
            ? 'Gửi email khôi phục mật khẩu thành công'
            : 'Gửi email khôi phục mật khẩu không thành công',
        });
      })
      .catch(err => {
        console.log('send reset password email error: ', err);
        res.status(500).send('Không thể gửi email khôi phục mật khẩu');
      });
    // userService
    //   .update(
    //     { email: get(req, ['body', 'email']), delete: false },
    //     { password: await hashPassword(get(req, ['body', 'newPassword'])) },
    //     ['id'],
    //   )
    //   .then(user => {
    //     if (user) {
    //       return res.status(200).json({ status: true, message: 'Reset mật khẩu thành công' });
    //     }
    //     res.status(200).json({ status: false, message: 'Reset mật khẩu thất bại' });
    //   });
  },
);

router.post(
  '/reset-password',
  validateDTO(ResetPasswordDTO),
  (req: any, res: any, next: any) => {
    userService
      .isExists({ authorizationToken: get(req, ['body', 'token']) }, 'id')
      .then(exists => {
        if (exists) {
          return next();
        }

        res.status(200).json({
          status: false,
          message: 'Thông tin token để khôi phục mật khẩu không đúng.',
        });
      })
      .catch(err => {
        console.log('reset password error: ', err);
        res.status(500).send('Không thể thực hiện lúc này.');
      });
  },
  async (req: any, res: any, next: any) => {
    const payload = jwt.verify(get(req, ['body', 'token']), JWTConfig.SECRET_KEY);
    userService
      .update(
        { id: get(payload, 'id') },
        {
          password: await hashPassword(get(req, ['body', 'newPassword'])),
          authorizationToken: null,
        },
        ['id'],
      )
      .then(user => {
        res.status(200).json({
          status: user ? true : false,
          message: user ? 'Khôi phục mật khẩu thành công' : 'Khôi phục mật khẩu không thành công',
        });
      })
      .catch(err => {
        console.log('reset password error: ', err);
        res.status(500).send('Không thể khôi phục mật khẩu lúc này');
      });
  },
);

export const authRouter = router;
