import { Router } from 'express';
import { get, omit, pick } from 'lodash';
import {
  alreadyAuth,
  checkLockAccount,
  checkAuthorizatedAccount,
  validateDTO,
  checkRole,
} from '../middlewares';
import { MTTRequest } from '../commons/defines/request';
import { userService, contractService } from '../services';
import { USER_ROLES } from '../commons/defines/enums';
import { CreateContractDTO, UpdateContractFinderDTO, UpdateContractTutorDTO } from '../dto';
import { commonConfigs } from '../commons/configs';
import {
  USER_FIELDS_NEED_TO_GET,
  USER_FIELDS_NEED_TO_REMOVE,
  CONTRACT_FIELDS_NEED_TO_GET,
} from '../commons/defines/constants';
import { checkTrustContractWithAccount } from '../middlewares/check-trust-contract-with-account.middleware';
import { In } from 'typeorm';

const router = Router();

router.get(
  '/me',
  alreadyAuth,
  checkLockAccount,
  checkAuthorizatedAccount,
  (req: MTTRequest, res: any, next: any) => {
    const page = parseInt(get(req, ['query', 'page'], 1));
    const pageSize = parseInt(get(req, ['query', 'pageSize'], commonConfigs.PAGE_SIZE));

    userService
      .getContracts(get(req, ['user', 'id']), page, pageSize)
      .then(contractsRes => res.status(200).json({ ...contractsRes }))
      .catch(err => {
        console.log('get contracts error: ', err);
        res.status(500).send('Không thể lấy dữ liệu');
      });
  },
);

router.get(
  '/me/:id',
  alreadyAuth,
  checkLockAccount,
  checkAuthorizatedAccount,
  checkTrustContractWithAccount,
  (req: MTTRequest, res: any, next: any) => {
    contractService
      .findOne({
        where: {
          id: get(req, ['params', 'id']),
          delete: false,
        },
        loadRelationIds: {
          relations: ['tutor', 'finder'],
        },
      })
      .then(async contract => {
        if (contract) {
          await userService
            .find({
              where: { id: In([contract.tutor, contract.finder]) },
              select: ['id', 'displayName', 'avatar', 'email', 'phone', 'role'],
              relations: ['address'],
            })
            .then(users => {
              console.log(
                'users',
                users.map(u => u.role),
              );
              const finderIndex = users.findIndex(user => user.role === USER_ROLES.FINDER);
              contract.finder = users[finderIndex];
              contract.tutor = users[finderIndex === 0 ? 1 : 0];
              console.log('contract', contract);
            });

          return res.status(200).json({ contract: omit(contract, ['delete']) });
        }

        res.status(200).json({ contract });
      });
  },
);

router.post(
  '/me',
  alreadyAuth,
  checkLockAccount,
  checkAuthorizatedAccount,
  checkRole(USER_ROLES.FINDER),
  validateDTO(CreateContractDTO),
  (req: MTTRequest, res: any, next: any) => {
    const data = req.body;

    userService
      .findOne({
        where: { id: get(req, ['body', 'tutorId']), role: USER_ROLES.TUTOR, delete: false },
        select: USER_FIELDS_NEED_TO_GET,
        relations: ['address'],
      })
      .then(tutor => {
        if (!tutor) {
          return res.status(200).json({
            status: false,
            contract: null,
            message: 'Gia sư bạn muốn đăng ký không tồn tại',
          });
        }

        contractService
          .save([
            {
              ...omit(data, ['tuition']),
              tuitionOfFinder: data.tuition || 0,
              tutor,
              finder: omit(get(req, 'user'), USER_FIELDS_NEED_TO_REMOVE),
            },
          ])
          .then(contracts => {
            res.status(200).json({
              status: true,
              contract: get(contracts, [0]),
              message: 'Đăng ký hợp đồng thành công',
            });
          })
          .catch(err => {
            console.log('create contract error: ', err);
            res.status(500).send('Không thể  đăng ký hợp đồng');
          });
      });
  },
);

router.post(
  '/me/:id/update-for-finder',
  alreadyAuth,
  checkLockAccount,
  checkAuthorizatedAccount,
  checkRole(USER_ROLES.FINDER),
  validateDTO(UpdateContractFinderDTO),
  (req: MTTRequest, res: any) => {
    const data = req.body;
    const contractId = parseInt(get(req, ['params', 'id']));

    contractService.isExists({ id: contractId, delete: false }, 'id').then(exists => {
      if (!exists) {
        return res
          .status(200)
          .json({ status: false, contract: null, message: 'Hợp đồng không hợp lệ' });
      }

      contractService.isValidToUpdate(contractId).then(valid => {
        if (!valid) {
          return res
            .status(200)
            .json({ status: false, contract: null, message: 'Hợp đồng không hợp lệ để cập nhật' });
        }
        contractService
          .update(
            { id: contractId },
            {
              ...omit(data, ['tuition']),
              ...(data.tuition ? { tuitionOfFinder: data.tuition } : {}),
            },
            CONTRACT_FIELDS_NEED_TO_GET,
            ['finder', 'tutor'],
          )
          .then(contract => {
            res.status(200).json({
              status: true,
              contract: {
                ...contract,
                tutor: omit(contract.tutor, USER_FIELDS_NEED_TO_REMOVE),
                finder: omit(contract.finder, USER_FIELDS_NEED_TO_REMOVE),
              },
              message: 'Cập nhật hợ đồng thành công',
            });
          })
          .catch(err => {
            console.log('update contract error: ', err);
            res.status(500).send('Không thể thực hiện');
          });
      });
    });
  },
);

router.post(
  '/me/:id/update-for-tutor',
  alreadyAuth,
  checkLockAccount,
  checkAuthorizatedAccount,
  checkRole(USER_ROLES.TUTOR),
  validateDTO(UpdateContractTutorDTO),
  (req: MTTRequest, res: any) => {
    const data = req.body;
    const contractId = parseInt(get(req, ['params', 'id']));

    contractService.isExists({ id: contractId, delete: false }, 'id').then(exists => {
      if (!exists) {
        return res
          .status(200)
          .json({ status: false, contract: null, message: 'Hợp đồng không hợp lệ' });
      }

      contractService.isValidToUpdate(contractId).then(valid => {
        if (!valid) {
          return res
            .status(200)
            .json({ status: false, contract: null, message: 'Hợp đồng không hợp lệ để cập nhật' });
        }
        contractService
          .update(
            { id: contractId },
            {
              ...omit(data, ['tuition']),
              ...(data.tuition ? { confirmedTuition: data.tuition } : {}),
            },
            CONTRACT_FIELDS_NEED_TO_GET,
            ['finder', 'tutor'],
          )
          .then(contract => {
            res.status(200).json({
              status: true,
              contract: {
                ...contract,
                tutor: omit(contract.tutor, USER_FIELDS_NEED_TO_REMOVE),
                finder: omit(contract.finder, USER_FIELDS_NEED_TO_REMOVE),
              },
              message: 'Cập nhật hợ đồng thành công',
            });
          })
          .catch(err => {
            console.log('update contract error: ', err);
            res.status(500).send('Không thể thực hiện');
          });
      });
    });
  },
);

router.post(
  '/:id/sign',
  alreadyAuth,
  checkLockAccount,
  checkAuthorizatedAccount,
  checkRole(USER_ROLES.FINDER, USER_ROLES.TUTOR),
  (req: any, res: any, next: any) => {
    contractService
      .isExists(
        {
          id: get(req, ['params', 'id']),
          ...(get(req, ['user', 'role']) === USER_ROLES.FINDER
            ? { finder: { id: get(req, ['user', 'id']) } }
            : { tutor: { id: get(req, ['user', 'id']) } }),
          delete: false,
        },
        'id',
      )
      .then(exists => {
        if (exists) {
          return next();
        }

        res.status(403).send('');
      })
      .catch(err => {
        console.log('sign contract error: ', err);
        res.status(500).send('Không thể thực hiện');
      });
  },
  async (req: any, res: any, next: any) => {
    const userId = get(req, ['user', 'id']);
    const role = get(req, ['user', 'role']);
    const contractId = get(req, ['params', 'id']);

    const contract = await contractService.findOne({
      where: { id: contractId },
      select: ['id', 'signRequestOfFinder', 'signRequestOfTutor', 'status'],
    });

    if (role === USER_ROLES.FINDER) {
      contract.signRequestOfFinder = true;
    } else {
      contract.signRequestOfTutor = true;
    }

    if (contract.signRequestOfFinder && contract.signRequestOfTutor) {
      contract.status = 1;
    }

    contractService
      .update(
        { id: contractId },
        omit(contract, ['id']),
        [
          'id',
          'signRequestOfFinder',
          'signRequestOfTutor',
          'status',
          'tuitionOfFinder',
          'confirmedTuition',
          'numberOfSession',
          'numberOfHourPerSession',
          'createdDate',
          'updatedDate',
        ],
        ['finder', 'tutor'],
      )
      .then(contract => {
        res.status(200).json({
          status: contract ? true : false,
          message: contract ? 'Đã ký hợp đồng' : 'Không ký được hợp đồng',
          contract,
        });
      })
      .catch(err => {
        console.log('sign contract error: ', err);
        res.status(500).send('Không thể thực hiện');
      });
  },
);

export const contractRouter = router;
