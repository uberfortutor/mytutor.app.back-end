import { Router } from 'express';
import { get } from 'lodash';
import { alreadyAuth, checkLockAccount, checkAuthorizatedAccount } from '../middlewares';
import { notificationService } from '../services';
const router = Router();

router.get(
  '/me',
  alreadyAuth,
  checkLockAccount,
  checkAuthorizatedAccount,
  (req: any, res: any, next: any) => {
    const page = get(req, ['query', 'page'], 1) || 1;
    const pageSize = get(req, ['query', 'pageSize'], 10) || 10;

    notificationService
      .find({
        where: {
          for: {
            id: get(req, ['user', 'id']),
          },
        },
        select: ['id', 'type', 'metadata'],
        take: pageSize,
        skip: (page - 1) * pageSize,
      })
      .then(notifications => {
        res.status(200).json({ notifications });
      })
      .catch(err => {
        console.log('get notifications list error: ', err);
        res.status(500).send('Không thể lấy');
      });
  },
);

export const notificationRouter = router;
