import { Router } from 'express';
import { mailService } from '../services/mail.service';

const router = Router();

router.get('/', (req, res, next) => {
  res.json({ AppName: 'mytutor.admin.back-end. hohoho' });
});

// router.post('/test-send-email', (req, res, next) => {
//   mailService
//     .mail(req.body.email, 'hello')
//     .then(info => {
//       res.json(info);
//     })
//     .catch(err => {
//       console.log('err', err);
//       res.send('error');
//     });
// });

export const defaultRouter = router;
