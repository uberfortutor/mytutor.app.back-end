import { Router } from 'express';
import { alreadyAuth, checkLockAccount, checkAuthorizatedAccount } from '../middlewares';
import { MTTError } from '../commons/defines/error';

const router = Router();

// NEED TO DO
router.get(
  '/me',
  alreadyAuth,
  checkLockAccount,
  checkAuthorizatedAccount,
  (req: MTTError, res: any, next: any) => {
    res.json({});
  },
);

export const incomingRouter = router;
