import * as express from 'express';
import * as expressSession from 'express-session';
import * as path from 'path';
import * as cookieParser from 'cookie-parser';
import * as logger from 'morgan';
import * as cors from 'cors';
import * as passport from 'passport';
import {
  defaultRouter,
  authRouter,
  tutorRouter,
  contractRouter,
  incomingRouter,
  notificationRouter,
} from './routes';
import { handleResponse } from './middlewares/response.middleware';
import { handleNotFound } from './middlewares/not-found.middleware';
import { handleError } from './middlewares/error.middleware';
import { configPassportStrategies } from './middlewares/passport';
import { connectDatabase } from './database/connect-database';
import { getConnection } from 'typeorm';
import { configSwagger } from './swagger';
import { skillRouter } from './routes/skill.route';

const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, '../public')));

//config express session
app.use(
  expressSession({
    secret: 'mytutor-1612772-1612523-1612810',
    resave: false,
    saveUninitialized: true,
    cookie: { secure: true, httpOnly: false },
  }),
);

// config passport
app.use(passport.initialize());
app.use(passport.session());
configPassportStrategies();
// config cors
app.use(cors());

// config swagger
configSwagger(app);

// config routes
app.use('/', defaultRouter);
app.use('/auth', authRouter);
app.use('/tutors', tutorRouter);
app.use('/contracts', contractRouter);
app.use('/incoming', incomingRouter);
app.use('/skills', skillRouter);
app.use('/notifications', notificationRouter);

// config middleware
app.use(handleResponse);
app.use(handleNotFound);
app.use(handleError);

// Connect to database
connectDatabase()
  .then(() => {
    const port = process.env.PORT || 3001;

    app.listen(port, () => {
      console.log('App is running on port ', port);
    });
  })
  .catch(err => console.log('can not run app'));
